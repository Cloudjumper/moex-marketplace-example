package org.fireplume.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "message")
public class KafkaMessageEntity {


        @Id
        @Column(name = "id")
        @GeneratedValue(strategy = GenerationType.AUTO, generator = "uuid_v4")
        //see DefaultIdentifierGeneratorFactory class for strategies
        @GenericGenerator(name = "uuid_v4", strategy = "uuid2",
                parameters = @Parameter(
                        name = "uuid_gen_strategy_class",
                        value = "org.fireplume.helper.generators.PostgreSQLUUIDGenerationStrategy"
                )
        )
        private UUID id;

        @Column(name = "data")
        private String data;

        @Column(name = "creation_date")
        private LocalDateTime creationDate;

        @Column(name = "topic")
        private String topic;

        @Column(name = "partition")
        private Integer partition;

        @Column(name = "key")
        private String key;
}
