package org.fireplume.kafka.consumer;

import org.fireplume.entity.KafkaMessageEntity;
import org.fireplume.repository.KafkaMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Service
public class KafkaConsumer {

    @Value("${kafka.topic}")
    private String topic;

    @Autowired
    private KafkaMessageRepository kafkaMessageRepository;

    @KafkaListener(topics = "${kafka.topic}", groupId = "${kafka.groupId}")
    public void listen(
            @Payload String message,
            @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition,
            @Header(KafkaHeaders.RECEIVED_TIMESTAMP) long receivedTimeStamp,
            @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) String key
    ) {
        final KafkaMessageEntity messageEntity =
                new KafkaMessageEntity(null, message, LocalDateTime.ofEpochSecond(receivedTimeStamp, 0, ZoneOffset.UTC),
                        topic, partition, key);
        kafkaMessageRepository.save(messageEntity);
        System.out.println("Received Message in group foo: " + message);
    }
}
