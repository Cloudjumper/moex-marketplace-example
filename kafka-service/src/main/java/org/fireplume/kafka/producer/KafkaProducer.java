package org.fireplume.kafka.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Service
public class KafkaProducer {

    @Value("${kafka.topic}")
    private String topic;

    @Value("${kafka.key}")
    private String key;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public void sendMessage(String msg) {
//        kafkaTemplate.send(topic, msg);
        kafkaTemplate.send(topic, 0, LocalDateTime.now().toEpochSecond(ZoneOffset.UTC), key, msg);
    }
}
