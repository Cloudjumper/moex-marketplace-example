package org.fireplume.repository;

import org.fireplume.entity.KafkaMessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface KafkaMessageRepository extends JpaRepository<KafkaMessageEntity, UUID> {
    List<KafkaMessageEntity> findAllByOrderByCreationDateDesc();
}
