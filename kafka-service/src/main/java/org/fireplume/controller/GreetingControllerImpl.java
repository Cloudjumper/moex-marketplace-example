package org.fireplume.controller;

import com.netflix.discovery.EurekaClient;
import org.fireplume.entity.KafkaMessageEntity;
import org.fireplume.kafka.producer.KafkaProducer;
import org.fireplume.repository.KafkaMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class GreetingControllerImpl {

    @Autowired
    private KafkaProducer kafkaProducer;

    @Autowired
    private KafkaMessageRepository kafkaMessageRepository;

    @Value("${spring.application.name}")
    private String appName;

    @PostMapping("/publish")
    public void publish(@RequestBody Map<String, String> body) {
        kafkaProducer.sendMessage(body.get("message"));
    }

    @GetMapping("/read")
    public List<KafkaMessageEntity> readAllMessages() {
        return kafkaMessageRepository.findAllByOrderByCreationDateDesc();
    }
}
