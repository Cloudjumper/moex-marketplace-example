CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE message
(
  id        UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
  data      TEXT                    NOT NULL,
  creation_date TIMESTAMP DEFAULT now() NOT NULL
);