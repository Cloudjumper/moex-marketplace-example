package org.fireplume.listener;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.springframework.stereotype.Service;

@Service
public class WeatherDetectorResultPrinter implements ExecutionListener {

    @Override
    public void notify(DelegateExecution execution) {
        System.out.println(execution.getProcessDefinitionId());
        System.out.println(execution.getProcessBusinessKey());
        System.out.println(execution.getProcessInstanceId());
        System.out.println(execution.getProcessInstance());
        System.out.println(execution.getVariable("weather"));
    }
}
