package org.fireplume;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;
import org.camunda.bpm.spring.boot.starter.event.PostDeployEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.event.EventListener;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
@EnableProcessApplication
public class CamundaApplication {
    @Autowired
    private RuntimeService runtimeService;

    /**
     * For test purpose, not work without proper bpmn file in resources
     * @param event
     */
    @EventListener
    private void processPostDeploy(PostDeployEvent event) {
//        runtimeService.startProcessInstanceByKey("loanApproval");
//        runtimeService.startProcessInstanceByKey("CheckWeather");
    }

    public static void main(String[] args) {
        SpringApplication.run(CamundaApplication.class, args);
    }
}
