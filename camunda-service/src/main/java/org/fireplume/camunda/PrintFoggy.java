package org.fireplume.camunda;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

public class PrintFoggy implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) {
        final String name = (String) execution.getVariable("name");
        final String weather = (String) execution.getVariable("weather");
        System.out.println(name + " said: The weather is \"" + weather + "\", I cant see anything");
    }
}
