package org.fireplume.camunda;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class WeatherDetector implements JavaDelegate {

    private static final List<String> WEATHER = new ArrayList<String>() {
        {
            add("Sunny"); add("Cloudy"); add("Foggy");
        }
    };

    @Override
    public void execute(DelegateExecution execution) throws InterruptedException {
        Thread.currentThread().sleep(10000L);
        execution.setVariable("name", System.getProperty("user.name"));
        execution.setVariable("weather", WEATHER.get(ThreadLocalRandom.current().nextInt(3)));
    }
}
