package org.fireplume.config;

import org.camunda.bpm.spring.boot.starter.rest.CamundaJerseyResourceConfig;
import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;

/**
 * Config file for camunda rest API
 *
 * now you can call http://localhost:9091/rest/engine/default/*
 *
 * example:
 * http://localhost:9091/rest/engine/default/authorization
 * http://localhost:9091/rest/engine/default/deployment/create
 */
@Component
@ApplicationPath("/rest")
public class JerseyConfig extends CamundaJerseyResourceConfig {
}
