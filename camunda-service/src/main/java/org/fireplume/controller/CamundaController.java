package org.fireplume.controller;

import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CamundaController {
    @Autowired
    private RuntimeService runtimeService;

    @GetMapping(path = "/camunda/start/{name}")
    public String startProcess(@PathVariable(name = "name") String name) {
        final ProcessInstanceWithVariables processInstanceWithVariables =
                runtimeService.createProcessInstanceByKey(name)
                .executeWithVariablesInReturn();
        return processInstanceWithVariables.getVariables().toString();
    }
}
