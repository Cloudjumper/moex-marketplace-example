package org.fireplume;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@Controller
public class ControllerImpl {
    @Autowired
    private Client client;

    @RequestMapping("/")
    public String greeting(Model model) {
        return "index";
    }

    @PostMapping("/publish")
    public ResponseEntity<?> publish(@RequestBody Map<String, String> body) {
        client.publish(body);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/readAll")
    public String readAllMessages(Model model) {
        model.addAttribute("messages", client.readAllMessages());
        return "table";
    }
}
