package org.fireplume;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import java.time.LocalDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class KafkaMessageDto {
        private UUID id;
        private String data;
        private LocalDateTime creationDate;
        private String topic;
        private Integer partition;
        private String key;
}
