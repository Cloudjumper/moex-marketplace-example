package org.fireplume;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@FeignClient("kafka-service")
public interface Client {

    @PostMapping(value = "/publish")
    void publish(@RequestBody Map<String, String> body);

    @GetMapping(value = "/read")
    List<KafkaMessageDto> readAllMessages();
}
